#include "host_io.h"

#include "TestSystem.h"

using namespace SDR;

static inline TestSystem * sys(HOST_IO_t * This){return (TestSystem *) This->cfg.Master;}

void init_HOST_IO(HOST_IO_t * This, HOST_IO_Cfg_t * Cfg)
{
  This->cfg = *Cfg;
}

void HOST_IO_setTargetState(HOST_IO_t * This, TARGET_State_t state)
{
  HOST_IO_send_state_response(This, state);
}


TARGET_State_t HOST_IO_getState(HOST_IO_t * This)
{
  if (HOST_IO_isStarted(This))
    return HOST_IO_isRunning(This) ? TARGET_Running : TARGET_Paused;
  return TARGET_Stopped;
}

Bool_t HOST_IO_isStarted(HOST_IO_t * This){return sys(This)->isStarted();}
Bool_t HOST_IO_isRunning(HOST_IO_t * This){return sys(This)->isRunning();}

Bool_t HOST_IO_send_uint8(HOST_IO_t * This, TARGET_DataId_t id, UInt8_t data, Size_t bitsCount){return false;}
Bool_t HOST_IO_send_data(HOST_IO_t * This, TARGET_DataId_t id, UInt8_t * Buf, Size_t Count){return false;}

Bool_t HOST_IO_send_response(HOST_IO_t * This, TARGET_Response_t Resp){return false;}

Bool_t HOST_IO_send_state_response(HOST_IO_t * This, TARGET_State_t State)
{
  switch(State)
  {
  case TARGET_Started:
    sys(This)->start(false);
    break;

  case TARGET_Running:
    sys(This)->start();
    break;

  case TARGET_Paused:
    sys(This)->pause();
    break;

  case TARGET_Stopped:
    sys(This)->stop();
    break;

  default:
    return false;
  }
  return true;
}

Bool_t HOST_IO_send_params(HOST_IO_t * This, TARGET_Params_t * params)
{
  sys(This)->ControlWidget()->ui_set_exec_timeout(params->timeout_ms);
  return true;
}

Bool_t HOST_IO_send_name(HOST_IO_t * This, const char * name)
{
  sys(This)->setName(name);
  return true;
}

Bool_t HOST_IO_send_format(HOST_IO_t * This, TARGET_DataId_t id, const char * format)
{
  switch(id)
  {
  case TARGET_controlFORMAT:
    sys(This)->setControlFormat(format);
    return true;

  case TARGET_statusFORMAT:
    sys(This)->setStatusFormat(format);
    return true;

  case TARGET_plotsFORMAT:
    sys(This)->setPlotsFormat(format);
    return true;

  default:
    return false;
  }
}

Bool_t HOST_IO_send_element_value(HOST_IO_t * This, TARGET_ElementType_t type, TARGET_DataId_t id, const char * val)
{
  switch(type)
  {
  case TARGET_ControlElement:
    if (!sys(This)->UserControlWidget()) return false;
    sys(This)->UserControlWidget()->setValue(id,val);
    break;
  case TARGET_StatusElement:
    if (!sys(This)->UserStatusWidget()) return false;
    sys(This)->UserStatusWidget()->setValue(id,val);
    break;
  default: return false;
  }
  return true;
}

Bool_t HOST_IO_send_element_params(HOST_IO_t * This, TARGET_ElementType_t type, TARGET_DataId_t id, const char * params)
{
  switch(type)
  {
  case TARGET_ControlElement:
    if (!sys(This)->UserControlWidget()) return false;
    sys(This)->UserControlWidget()->setParams(id,params);
    break;
  case TARGET_StatusElement:
    if (!sys(This)->UserStatusWidget()) return false;
    sys(This)->UserStatusWidget()->setParams(id,params);
    break;
  default: return false;
  }
  return true;
}

Bool_t HOST_IO_send_plot_command(HOST_IO_t * This, TARGET_DataId_t plotId, TARGET_PlotCommand_t cmd)
{
  QList<TPlot*> Plots = sys(This)->plotsmap()->values(plotId);
  foreach(TPlot* plot, Plots)
  {
    switch(cmd)
    {
    case TARGET_plotCmd_Clear:
      plot->clear();
      break;
    default:
      return false;
    }
  }
  return true;
}
Bool_t HOST_IO_send_plot_command_ext(HOST_IO_t * This, TARGET_DataId_t plotId, TARGET_PlotCommand_t cmd, const char * ext)
{
  QList<TPlot*> Plots = sys(This)->plotsmap()->values(plotId);
  foreach(TPlot* plot, Plots)
  {
    switch(cmd)
    {
    case TARGET_plotCmd_setParam:
      plot->readParams(ext);
      break;
    default:
      return false;
    }
  }
  return true;
}

Bool_t HOST_IO_send_samples(HOST_IO_t * This, TARGET_DataId_t id, Sample_t * Buf, Size_t Count)
{
  QList<TPlot*> Plots = sys(This)->plotsmap()->values(id);
  foreach(TPlot* plot, Plots)
    plot->append(Buf,Count);
  return true;
}

Bool_t HOST_IO_send_samples_iq(HOST_IO_t * This, TARGET_DataId_t id, iqSample_t * Buf, Size_t Count)
{
  QList<TPlot*> values = sys(This)->plotsmap()->values(id);
  foreach(TPlot* plot, values)
    plot->append(Buf,Count);
  return true;
}

Bool_t HOST_IO_send_log_message(HOST_IO_t * This, const char * message)
{
  sys(This)->ControlWidget()->Logs()->append(message);
  return true;
}

void HOST_IO_startPaused(HOST_IO_t * This){sys(This)->start(false);}
void HOST_IO_start(HOST_IO_t * This){sys(This)->start();}
void HOST_IO_pause(HOST_IO_t * This){sys(This)->pause();}
void HOST_IO_continue(HOST_IO_t * This){sys(This)->start();}
void HOST_IO_stop(HOST_IO_t * This){sys(This)->stop();}
void HOST_IO_step(HOST_IO_t * This){sys(This)->step();}
void HOST_IO_reset(HOST_IO_t * This){sys(This)->reset();}

