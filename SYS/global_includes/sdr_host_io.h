#ifndef TESTAPP_SYS_SDR_HOST_IO_H_
#define TESTAPP_SYS_SDR_HOST_IO_H_

#include "../host_io.h"

#ifdef __cplusplus
extern "C"{
#endif

HOST_IO_t * HOST_IO();

#ifdef __cplusplus
}
#endif

#endif /* TESTAPP_SYS_SDR_HOST_IO_H_ */
