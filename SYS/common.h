/*
 * common.h
 *
 *  Created on: 22 янв. 2018 г.
 *      Author: svetozar
 */

#ifndef SYS_COMMON_H_
#define SYS_COMMON_H_

#include <stdint.h>
#include <stdbool.h>

#define SYS_INLINE static inline

#ifndef SYS_ASSERT
#include <assert.h>
#define SYS_ASSERT(x) assert(x)
#endif

#ifdef __cplusplus
extern "C"{
#endif

#ifdef __cplusplus
}
#endif

#endif /* SYS_COMMON_H_ */
