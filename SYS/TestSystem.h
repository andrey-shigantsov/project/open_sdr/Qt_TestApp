#ifndef TESTSYSTEM_H
#define TESTSYSTEM_H

#include "../TestApp.h"
#include <SDR/TestBase/TestData.h>

#include "host_io.h"
#include <SDR/Qt_Addons/Controls/TControlWidget.h>
#include <SDR/Qt_Addons/Controls/TStatusWidget.h>

#include <QMap>

namespace SDR
{
  class TestSystem : public TestApp
  {
    Q_OBJECT

  public:
    explicit TestSystem(TestData_t * test, int timeout_ms);

    void setAsGlobalIO();
    HOST_IO_t * IO(){return &io;}

    TestData_t * data(){return d;}

    QMap<int, TPlot *> * plotsmap(){return &PlotsMap;}

    TControlWidget * UserControlWidget(){return ucw;}
    TStatusWidget * UserStatusWidget(){return usw;}

    void setControlFormat(const char * format);
    void setStatusFormat(const char * format);
    void setPlotsFormat(const char * format);

  public slots:
    void init(){TestApp::init();}
    void prestart(){testdata_prestart(d);}
    void reset(){TestApp::reset();testdata_reset(d);}
    void test(){testdata_exec(d);}
    void prestop(){testdata_prestop(d);testdata_sys_destroy(d);}

  private:
    HOST_IO_t io;
    TestData_t * d;

    TControlWidget * ucw;
    TStatusWidget * usw;
    QMap<int, TPlot *> PlotsMap;

  private slots:
    void refresh_exec_state(ExecState state);
    void setControlElementValue(int id, QString val);
  };
}

#endif // TESTSYSTEM_H
