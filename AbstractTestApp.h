#ifndef ABSTRACTTESTAPP_H
#define ABSTRACTTESTAPP_H

#include <QObject>
#include <QTimer>

namespace SDR
{

class TestControl;

class AbstractTestApp : public QObject
{
  Q_OBJECT
public:
  enum ExecState {Stopped, Started, Running};

  AbstractTestApp(int timeout_ms, QString name);
  virtual ~AbstractTestApp();

  void setName(QString name);

  inline TestControl* ControlWidget(){return CtrlWidget;}
  void enableDeleteOnCloseControlWidget();

  double TimeoutS(){return (double)TimeoutMs/1000;}

  virtual void setTimeout(int ms){TimeoutMs = ms;}

signals:
  void exec_state_changed(ExecState state);

public slots:
  virtual void init() = 0;
  virtual void start() = 0;
  virtual void pause() = 0;
  virtual void stop() = 0;
  virtual void step() = 0;
  virtual void reset() = 0;

protected:
  TestControl* CtrlWidget;

  int TimeoutMs;

  void setExecState(ExecState state);
};

} // SDR

#include "TestControl.h"

#endif // ABSTRACTTESTAPP_H
