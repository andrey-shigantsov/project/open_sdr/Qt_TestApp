#include "AbstractTestApp.h"
#include "TestControl.h"

using namespace SDR;

AbstractTestApp::AbstractTestApp(int timeout_ms, QString name) :
  QObject(0)
{
  CtrlWidget = new TestControl(this, timeout_ms, name);

  setTimeout(timeout_ms);

  CtrlWidget->ui_set_exec_state(Stopped);
}

AbstractTestApp::~AbstractTestApp()
{
  delete CtrlWidget;
}

void AbstractTestApp::setName(QString name)
{
  CtrlWidget->setName(name);
}

void AbstractTestApp::enableDeleteOnCloseControlWidget()
{
  CtrlWidget->setAttribute(Qt::WA_DeleteOnClose, true);
  connect(CtrlWidget, SIGNAL(closed()), this, SLOT(deleteLater()));
}

void AbstractTestApp::setExecState(AbstractTestApp::ExecState state)
{
  CtrlWidget->ui_set_exec_state(state);
  emit exec_state_changed(state);
}
